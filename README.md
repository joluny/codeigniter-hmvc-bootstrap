#Codeigniter HMVC Bootstrap
The idea is to have you up and running with minimum or NO configuration.

* Just point your web server to the "public" folder and you should be good to go.
* Auto detects http or https, just use base_url() to dynamically get the base URL, or disable it in config.php

###Codeigniter Version 2.2.1

Start Codeigniter with module, Auth Support and more:

* HMVC
* REST Library
* TEMPLATE Library
* CRUD (MY_Model - core)
* S3
* ion_auth Library
* CURL Library

Frontend ( Initializr ):

* jQuery (v1.10.1)
* Modernizr (v2.6.2)
* Twitter Bootstrap (v3.0.0)