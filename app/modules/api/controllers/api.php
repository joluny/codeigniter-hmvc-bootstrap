<?php
/**
 * Sample REST Controller
 */
class Api extends REST_Controller{
	
	public function __Construct()
	{
		parent::__Construct();
		$this->load->model('api_model');
	}
    
    public function data_post()
    {
        $post_data = $this->post();

        $return = $this->api_model->create_something($post_data);

        $this->response($return, 200);
    }

	public function data_get()
	{
		$get_data = $this->get();

		$return = $this->api_model->get_something($get_data);

		$this->response($return, 200);
	}
    
    public function data_put()
    {
        $put_data = $this->put();

        $return = $this->api_model->update_something($put_data);

        $this->response($return, 200);
    }
    
    public function data_delete()
    {
        $delete_data = $this->delete();

        $return = $this->api_model->delete_something($delete_data);

        $this->response($return, 200);
    }
	
}