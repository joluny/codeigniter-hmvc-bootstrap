<?php
/**
 * Sample Controller
 */
class Sample extends CI_Controller {
	
	public function __Construct() {
		parent::__Construct();
		
		$this->template->set_layout('sample');
	}
	
	public function index()
    {
        $data['test_data'] = 'This is sample data.';
        
		$this->template->build('sample_view', $data);
    }

}